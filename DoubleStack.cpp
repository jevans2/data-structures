// DoubleStack.cpp

#include "DoubleStack.h"
#include <iostream>


// Default constructor, the stack is empty to start
DoubleStack::DoubleStack() {
// Insert your code here
    
    a=-1;
    b=19;
    length=0;
}


// Default destructor
DoubleStack::~DoubleStack() {
// Insert your code here 
   
}

// Add "value" to the top of stack A
void DoubleStack::PushA(char value) {
// Insert your code here
    if(a>19)
    {
        throw(404); //throw error if a is greater than 19
    }
    else if(length<0||length>=20)
    {
        throw(404); //throws error is length is less than 0, or greater than 20
    }
    else
    {
        Dubs[a]=value;  //Assigns value to location Dubs[a]
        a++;    //increments a
        length++;   //increments length
    }
}

// Add "value" to the top of stack 
void DoubleStack::PushB(char value) {
// Insert your code here
    
    if(b<0)
    {
        throw(404); //throws if b is less than 0
    }
    else if(length<0||length>=20)
    {
        throw(404); //throws if length is less than 0 or greater than 20
    }
    else
    {
        Dubs[b]=value;  //sets the location at Dubs[b] equal to value
        b--;    //decrements b
        length++;   //increments length
        
    }
    
}


// Remove and return the item on the top of stack A
char DoubleStack::PopA() {
// Insert your code here
    if(a<1)
    {
        throw(404); //Throws error is a is less than 1
    }
    else if(length<=0)
    {
        throw(404); //Throws error if length is less than or equal to 0
    }
    else
    {
        length--;   //decrements length
        return(Dubs[a-1]);  //returns the value at location Dubs[a-1]
        
        a--;    //decrements a
        
        
        
    }
}

// Remove and return the item on the top of stack B
char DoubleStack::PopB() {
// Insert your code here
    
        if(b>19)
    {
        throw(404); //Throws error is b is greater than 19
    }
        else if(length<=0)
    {
        throw(404); //throws error is length is less than or equal to 0
    }
        else
    {
        length--;    //decrements length
        return(Dubs[b+1]);  //returns the value at location Dubs[b+1]
        
        b++;    //increments b
        
        
    }
}

// Return the item on the top of stack A
char DoubleStack::TopA() {
// Insert your code here
    return(Dubs[a-1]);   //returns the value at location Dubs[a-1]
}

// Return the item on the top of stack B
char DoubleStack::TopB() {
// Insert your code here
    return(Dubs[b+1]);   //returns the value at location Dubs[b+1]
}

// Return the number of items in the stack
unsigned int DoubleStack::size() {
// Insert your code here
    return(length); //returns the number of items in the array
}