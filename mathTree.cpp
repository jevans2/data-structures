// mathTree.cpp
#include <iostream>
#include <sstream>
#include "mathTree.h"
#include <string>

using namespace std;

string stringify(long double x) {
    ostringstream o;
    if (!(o << x))
        throw;
    return o.str();
}

mathTree::mathTree() {
    // Add content here
    treeNode *leaf;
    treeNode *parent;
}

mathTree::~mathTree() {
    // Add content here

}

void mathTree::ReadExpression(string s) { //reads tree in prefix notation, if it only has one value, returns that value, preorder traversal
    // Add content here
    string whatsleft;

    root = NULL; //sets root to NULL
    _readex(root, s); //runs readex
    whatsleft = _readex(root, s); //sets whats left equal to what is returned from readex
}

string mathTree::_readex(treeNode* t, string s) {
    // Add content here
    string remaining, firstpart;
    if (remaining.length() == 0 || t == NULL) //checks if length remaining in the string is 0, or that t is NULL
    {
        throw (404); //throws if length is 0 or t is NULL
    }
    if (firstpart == "+" || "*") //checks if the first part of the string is an operator
    {
        t->leafNode = false; //sets leafnode to false
        t->op = firstpart[0]; //sets op to the first character in first part
        t->lChild = new treeNode; //new left child 
        t->rChild = new treeNode; //new right child
        remaining = _readex(t->rChild, remaining); //remaining is what is left after readex runs again
    } else {
        t->leafNode = true; //sets leaf node to truw
        t->lChild = NULL; //sets left child to NULL
        t->rChild = NULL; //sets right child to NULL
        return remaining; //returns remaining
    }

}

double mathTree::ExpressionValue() { //called from root to evaluate entire tree
    // Add content here

    if (root == NULL) {
        throw (404); //throws if root is NULL
    }        /*else if (leafNode==true)
    { 
        return;
    }*/
    else {

    }

}

double mathTree::_evaluate(treeNode* t) { //evaluate tree
    // Add content here
    if (t == NULL) //checks if NULL
    {
        throw (404); //throws if t is NULL
    }

}

string mathTree::ReturnInfix() { //reads tree in infix notation, returns infix notation, inorder traversal
    // Add content here
    string val;

}

string mathTree::_inorder(treeNode* t) //inorder traversal
{
    // Add content here
    if (t == NULL) //base case
    {
        return (NULL); //returns NULL at base case
    }
    _inorder(t->lChild); //traverses inorder at left child
    cout << t->op << endl; //outputs operator at t
    _inorder(t->rChild); //traverses inorder at right child

}
