// LinkedList.cpp
//Got help from geeksforgeeks.org for getNode Function and Reverse function
//Also received logic help from Sal Picheria and Ryan Hanley
#include "LinkedList.h"
#include <cstdlib>
#include <iostream>

using namespace std;



LinkedList::LinkedList() {
    head=NULL;  /*initializes head to NULL*/
    length=0;    //initializes length at 0
                 
}

// Default destructor, must delete all nodes

LinkedList::~LinkedList() {
    for(int i=0;i<length;i++)
    {
    if(head=NULL) //checks if head is NULL
    {
        throw(404); //throws if head is NULL        
    }
    DeleteNode(0);  //while the length is 0, delete node at index 0
    }
        
    
    
}

// Add a node containing "value" to the front

void LinkedList::InsertFront(double value) {
    Node* thatGuy;  //creates a node pointer called thatGuy
    thatGuy=new Node;   //initializes thatGuy as a new node
    thatGuy->data=value;    //sets thatGuy's data equal to value
    thatGuy->next=head;     //sets thatGuy as the head pointer
    head=thatGuy;   //" "
    length++;   //increments length
}

// Add a node containing "value" to position "index"

void LinkedList::Insert(double value, unsigned int index) {
    Node* thatGuy, *prev;   //creates 2 node pointers, thatGuy and prev
    thatGuy=new Node;       //creates a new node called thatGuy
    prev=head;              //sets node previous to thatGuy equal to head
    if(index>length)
    {
        throw(404);     //throws 404 if the length is less than 0 or the index is greater than the length
    }
    else if(index==0)    
    {
        InsertFront(value); //calls the insert function if the index is 0
        
    }
    else
    {
    for(int z=0;z<index-1;z++)    //increments z starting at 0 for as long as it is less than index
    {
        prev=prev->next;    //prev points to the next node
    }
    thatGuy->data=value;    //sets thatGuy's data equal to value
    thatGuy->next=prev->next;   //thatGuy is the next node
    prev->next=thatGuy;     //sets the next node equal to thatGuy
    length++;   //increments length by 1
    }
    
    

}


// Return value at position "index"
//Got help from geeksforgeeks.org for getNode Function and Reverse function, gave me the idea for return statement
double LinkedList::GetNode(unsigned int index) {

    Node* cur=head; //creates a node cur and sets it equal to head
    
           
    
         if(index<0) //checks if index is 0
        {
            throw (404);    //throws error message 404 if index is less than 0
        }
    
        else if(index>length-1)
        {
            throw(404); //if index is greater than length, throw error code 404
        }
    
    else    
        {
        for(int y=0;y<index;y++)
        {
            cur=cur->next;  //sets cur pointing to the next node
            
        }
        return(cur->data);  //returns data, which is the position of cur
    }
        
    
       
}

    

    
    
     
        //check to see if index is legal, if illegal, throw out
        //move a pointer to the node the user is requesting
        //return data at that pointer
        
  




// Return the index of the node containing "value"

unsigned int LinkedList::Search(double value) {
    
    Node* cur=head; //creates a node cur and sets it equal to head
    
        if(cur==NULL)   //checks if cur is equal to null
        {
            throw (404);    //throws error code 404
        }
        else if(value<0)
        {
            throw(404); //throws error message 404if value is less than 0
        }
        else 
        {
            int m=0;    //declares a variable m and initializes it at 0
            for(int y=0;y<=length;y++)
        {
            while(cur->data!=value) //while the data at curis not equal to the value passed through
            {
            cur=cur->next;      //sets cur pointing to the next node
            m++;    //increment m
            }
            
        }
        return(m);  //returns data, which is the position of cur
        
        
}
}

// Delete the node at position "index", return the value that was there

double LinkedList::DeleteNode(unsigned int index) {
    Node* prev;     //creates a node called prev
    Node* temp;     //creates a node called temp
    double hs=0;    //intitializes variable hs at 0
    prev=head; 
    if(index>length-1)    //sets prev equal to head
    {
        throw(404);     //throws error message 404
    }
    else if(index==0)
    {
        prev->next=temp;    //sets next equal to temp
    }
    for(int x=0;x<index-1;x++)
    {
        prev=prev->next;    //sets prev equal to next
    }
    temp=prev->next->next; //sets temp pointing to next which points to the next node
    
    prev->next=temp->next;
    hs=temp->data;      //sets hs equal to the data found at temp
    delete prev->next;     //deletes the next node
    
    length--;       //decrements length
    return(hs);     //returns hs
}

// This function reverses the order of all nodes so the first node is now the
// last and the last is now the first (and all between). So a list containing 
// (4, 0, 2, 11, 29) would be (29, 11, 2, 0, 4) after Reverse() is called.
/*Got help from geeksforgeeks.org for getNode Function and Reverse function, used for figuring
out where to start on reverse function*/
void LinkedList::Reverse() {
    Node* prev=NULL;        //creates a node called prev and sets it equal to null
    Node* cur=head;     //creates a node cur and sets it equal to head
    Node* next;     //creates a node called next
    while(cur!=NULL)
    {
        next=cur->next;     //sets cur equal to next and pointing to it
        cur->next=prev;     //sets the value at next equal to prev
        prev=cur;           //sets prev equal to cur
        cur=next;           //sets cur equal to next
    }
    head=prev;      //sets head equal to prev
}

// Return the number of nodes in the list

int LinkedList::GetLength() {

   return(length);  //returns the length
}