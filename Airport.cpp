// Airport.cpp
//Got help at http://www.cprogramming.com/tutorial/modulus.html to figure out modulus operator in C++
#include "Airport.h"
#include <queue>
#include <cstdlib>
#include <iostream>

using namespace std;

void Airport(
  int landingTime,    // Time segments needed for one plane to land
  int takeoffTime,    // Time segs. needed for one plane to take off
  double arrivalProb, // Probability that a plane will arrive in
                      // any given segment to the landing queue
  double takeoffProb, // Probability that a plane will arrive in
                      // any given segment to the takeoff queue
  int maxTime,        // Maximum number of time segments that a plane
                      // can stay in the landing queue
  int simulationLength// Total number of time segs. to simulate
)
{
// Insert content here
    queue <double> takeoff; //queue for planes waiting to take off
    queue <double> landing; //queue for planes waiting to land
    double landings=0;  
    double takeoffs=0;
    unsigned int sizeA=0;   //size for landing queue
    unsigned int sizeB=0;   //size for takeoff queue
    unsigned int crash=0;   //crash counter
    unsigned int LandTime=0;  //counter for landing time
    double average;
    unsigned int TakeTime=0;  //counter for takeoff time
    for(int clock=0;clock<simulationLength;clock++)
    {
        if(rand()<(RAND_MAX*arrivalProb))   //checks if random number is less than arrival probability multiplied by max random number
        {
            
            landing.push(0);    //pushes 0 into landing queue
            sizeA++;            //increments size of first queue
            cout<<"Plane arrives at time "<<clock<<endl;
        }
        else if(rand()<(RAND_MAX*takeoffProb))  //checks if random number is less than takeoff probability times the max random number
        {
            if(sizeA==0&&sizeB==0)  //checks if both queues are empty
            {
                cout<<"Runway Empty"<<endl;
                continue;
            }
            else if(sizeA==0)   //checks if just the first queue is empty
            {
                continue;
            }
            else
            {
                landings++;     //increments landing counter
                landing.pop();  //removes the front of the landing queue
               
                takeoff.push(0);    //pushes into takeoff queue
                sizeA--;    //decrements first queue
                sizeB++;    //increments second queue
                cout<<"Plane lands at time "<<clock<<endl;
            }
           
        }
        
        else if(sizeA!=0)
        {
            LandTime++; 
            if(LandTime%maxTime==0) //checks if LandTime is a multiple of maxTime
            {
                landing.pop();      //removes the front of the landing queue
                sizeA--;            //decrements first queue
                crash++;            //increments crash counter
                cout<<"Plane Crashed"<<endl;
            }
        }
        else if(sizeB!=0)
        {
            TakeTime++;
            if(TakeTime%maxTime==0) //checks if takeTime is a multiple of maxTime
            {
                takeoff.pop();      //removes front of takeoff queue
                sizeB--;            //decrements second queue
                takeoffs++;         //increments takeoffs
                cout<<"Succesful takeoff"<<endl;
            }
        }
    }
    
    cout<<"Number of crashed planes: "<<crash<<endl;
    cout<<"Number of succesfully landed planes: "<<landings<<endl;
    cout<<"Number of planes that took off: "<<takeoffs<<endl;
    cout<<"Number of planes in landing queue: "<<sizeA<<endl;
    cout<<"Number of planes in takeoff queue: "<<sizeB<<endl;
    average=landings/landingTime;                       //makes average equal to the number of landings divided by the landing time
    cout<<"Average wait time for landing: "<<average<<endl;
    average=takeoffs/takeoffTime;                       //makes average equal to the number of takeoffs divided by the takeoff time
    cout<<"Average wait time for takeoff: "<<average<<endl;
            
    }